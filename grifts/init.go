package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/yroot/kickrtackr/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
